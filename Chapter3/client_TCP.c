#if defined(_WIN32)
#if !defined(IPV6_V6ONLY)
#define IPV6_V6ONLY 27
#endif
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#include <conio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


#if defined(_WIN32)
#define ISVALIDSOCKET(s) ((s) != INVALID_SOCKET)
#define CLOSESOCKET(s) closesocket(s)
#define GETSOCKETERRNO() (WSAGetLastError())
#else
#define ISVALIDSOCKET(s) ((s) >= 0)
#define CLOSESOCKET(s) close(s)
#define SOCKET int
#define GETSOCKETERRNO() (errno)
#endif


#define PORT "8080"

int main (int argc, char *argv[])
{
    /* Check command-line argument condition */
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s <host name> <port>\n", argv[0]);
        return 1;
    }
    /* Initialize Winsock */
    #if defined(_WIN32)
    WSADATA d;
    if (WSAStartup(MAKEWORD(2, 2), &d))
    {
        fprintf(stderr, "Failed to initialize.\n");
        return 1;
    }
    #endif

    /* Get user name */
    char myname[200];
    printf("Enter your name: ");
    gets(myname);
    fflush(stdin);

    /* Configure out the local address */
    printf("Configuring remote address...\n");
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_socktype = SOCK_STREAM;                // using TCP
    struct addrinfo *peer_address;
    if (getaddrinfo(argv[1], argv[2], &hints, &peer_address))
    {
        fprintf(stderr, "getaddrinfo() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }

    /* Display remote address information */
    printf("Remote address is: ");
    char address_buffer[100];
    char service_buffer[200];
    getnameinfo(peer_address->ai_addr, peer_address->ai_addrlen, address_buffer, sizeof(address_buffer), service_buffer, sizeof(service_buffer), NI_NUMERICHOST);
    printf("%s %s\n", address_buffer, service_buffer);


    /* Creat the socket */
    printf("Creating socket ...\n");
    SOCKET socket_peer = socket(peer_address->ai_family, peer_address->ai_socktype, peer_address->ai_protocol);

    /* Check to call socket() successful */
    if (!ISVALIDSOCKET(socket_peer))
    {
        fprintf(stderr, "socket() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }

    
    /* connect() to establish a connection to the remote server */
    printf("Connecting ...\n");
    if (connect(socket_peer, peer_address->ai_addr, peer_address->ai_addrlen))
    {
        fprintf(stderr, "connect() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }
    printf("Connected\n");
    freeaddrinfo(peer_address);
    /* Get my ip address and port */
    unsigned int myPort;
    struct sockaddr_in my_address;
    socklen_t len = sizeof(my_address);
    getsockname(socket_peer, (struct sockaddr *) &my_address, &len);
    char *myIP = inet_ntoa(my_address.sin_addr);
    myPort = ntohs(my_address.sin_port);
    printf("Local IP address: %s\nLocal PORT: %u\n", myIP, myPort); 

    /* check both the terminal and socket for new data */
    /* if new data come from terminal, we send it to the socket */
    /* if new data come from socket, we print it to the terminal */
    for (;;)
    {
        static int count = 0;
        fd_set reads;
        FD_ZERO(&reads);
        FD_SET(socket_peer, &reads);
        #if !defined(_WIN32)
        FD_SET(0, &reads);
        #endif

        struct timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 100000;

        if (select(socket_peer+1, &reads, 0, 0, &timeout) < 0)
        {
            fprintf(stderr, "select() failed. (%d)\n", GETSOCKETERRNO());
            return 1;
        } 

        /* checking for new TCP data */
        if (FD_ISSET(socket_peer, &reads))
        {
            char buffer_read[4096];
            int bytes_received = recv(socket_peer, buffer_read, 4096, 0);
            if (bytes_received < 1)
            {
                printf("Connection closed by peer.\n");
                break;
            }
            printf("\n%.*s", bytes_received, buffer_read);
            printf("Me > ");
            //printf("\nReceived (%d bytes): %.*s", bytes_received, bytes_received, buffer_read);
            //printf("%s %u > ", myIP, myPort);
        }

        if (count == 0)
        {
            printf("Me > ");
            //printf("%s %u > ", myIP, myPort);
            count = 1;
        }
        /* check for terminal input */
        #if defined (_WIN32)
        if (_kbhit())
        {
        #else
        if (FD_ISSET(0, &reads))
        {
        #endif
            count = 0;
            char total[4096 + 200];
            char read[4096];
            if (!fgets(read, 4096, stdin))
                break;
            memset(total, 0, sizeof(total));
            strcat(total, myname);
            strcat(total, " > ");
            strcat(total, read);
            int bytes_sent = send(socket_peer, total, strlen(total), 0);
            //printf("Sending: %s", read);
            //int bytes_sent = send(socket_peer, read, strlen(read), 0);
            //printf("Sent %d bytes.\n", bytes_sent);
        }
    }
    printf("Closing socket ...\n");
    CLOSESOCKET(socket_peer);

    #if defined(_WIN32)
    WSACleanup();
    #endif

    printf("Finished.\n");

    return 0;
}