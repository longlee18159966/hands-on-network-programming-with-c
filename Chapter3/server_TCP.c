#if defined(_WIN32)
#if !defined(IPV6_V6ONLY)
#define IPV6_V6ONLY 27
#endif
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

#if defined(_WIN32)
#define ISVALIDSOCKET(s) ((s) != INVALID_SOCKET)
#define CLOSESOCKET(s) closesocket(s)
#define GETSOCKETERRNO() (WSAGetLastError())
#else
#define ISVALIDSOCKET(s) ((s) >= 0)
#define CLOSESOCKET(s) close(s)
#define SOCKET int
#define GETSOCKETERRNO() (errno)
#endif


#define PORT "8080"

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s <host name> <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    /* Initialize Winsock */
    #if defined(_WIN32)
    WSADATA d;
    if (WSAStartup(MAKEWORD(2, 2), &d))
    {
        fprintf(stderr, "Failed to initialize.\n");
        return 1;
    }
    #endif

    /* Configure out the local address */
    printf("Configuring local address...\n");
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;                      // IPv4 address
    hints.ai_socktype = SOCK_STREAM;                // using TCP
    hints.ai_flags = AI_PASSIVE;                    // listen on any vailable network
    struct addrinfo *bind_address;
    getaddrinfo(argv[1], argv[2], &hints, &bind_address);
    
    /* Creat the socket */
    printf("Creating socket ...\n");
    SOCKET socket_listen = socket(bind_address->ai_family, bind_address->ai_socktype, bind_address->ai_protocol);

    /* Check to call socket() successful */
    if (!ISVALIDSOCKET(socket_listen))
    {
        fprintf(stderr, "socket() failed. (%d)\n", GETSOCKETERRNO());

        return 1;
    }

   
    /* bind() socket to associate address */
    printf("Binding socket to local address ...\n");
    if (bind(socket_listen, bind_address->ai_addr, bind_address->ai_addrlen))
    {
        fprintf(stderr, "bind() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }   
    freeaddrinfo(bind_address);
    
    /* Listen */
    printf("Listening ...\n");
    if (listen(socket_listen, 10) < 0)
    {
        fprintf(stderr, "listen() failed. (%d)\n", GETSOCKETERRNO());
        return 1;
    }

    /* store all socket (both active and inactive) */
    fd_set master;
    FD_ZERO(&master);
    FD_SET(socket_listen, &master);
    SOCKET max_socket = socket_listen;

    /* add new connection to master as they are established */
    printf("Waiting for connection...\n");
    for (;;)
    {
        fd_set reads;
        reads = master;
        if (select(max_socket+1, &reads, 0, 0, 0) < 0)
        {
            fprintf(stderr, "select() failed. (%d)\n", GETSOCKETERRNO());
            return 1;
        }
        SOCKET i;
        for (i = 1; i <= max_socket; ++i)
        {
            if (FD_ISSET(i, &reads))
            {
                /* Handle socket */
                if (i == socket_listen)
                {
                    struct sockaddr_storage client_address;
                    socklen_t client_len = sizeof(client_address);
                    SOCKET socket_client = accept(socket_listen, (struct sockaddr*) &client_address, &client_len);
                    if (!ISVALIDSOCKET(socket_client))
                    {
                        fprintf(stderr, "accept() failed. (%d)\n", GETSOCKETERRNO());
                        return 1;
                    }

                    FD_SET(socket_client, &master);
                    if (socket_client > max_socket)
                        max_socket = socket_client;
                    
                    char addressIP_buffer[100];
                    char port_buffer[100];
                    getnameinfo((struct sockaddr *) &client_address, client_len, addressIP_buffer, sizeof(addressIP_buffer), port_buffer, sizeof(port_buffer), NI_NUMERICHOST);
                    printf("New connection from %s %s\n", addressIP_buffer, port_buffer);
                }
                else
                {
                    char read[1024];
                    int bytes_received = recv(i, read, 1024, 0);
                    if (bytes_received < 1)
                    {
                        FD_CLR(i, &master);
                        CLOSESOCKET(i);
                        continue;
                    }   
                    //#define normal
                    #define chatroom
                    
                    #ifdef normal
                    int j;
                    for (j = 0; j < bytes_received; ++j)
                        read[j] = toupper(read[j]);
                    send(i, read, bytes_received, 0);
                    #endif

                    #ifdef chatroom
                    SOCKET j;
                    for (j = 1; j <= max_socket; ++j)
                    {
                        if (FD_ISSET(j, &master))
                        {
                            if ((j == socket_listen) || (j == i))
                                continue;
                            else
                                send(j, read, bytes_received, 0);
                        }
                    }
                    #endif
                }
            }   /* end if FD_ISSET */
        }       /* end for i to max_socket */   
    }           /* end for(;;) */

    #if defined(_WIN32)
    WSACleanup();
    #endif

    printf("Finished.\n");

    return 0;
}
