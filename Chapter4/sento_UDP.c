#if defined(_WIN32)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#include <winsock2.h>
#include <ws2tcpip.h>

#define ISVALIDSOCKET(s)        ((s) != INVALID_SOCKET)
#define CLOSESOCKET(s)          closesocket(s)
#define GETSOCKETERROR()        GetLastError()

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#define ISVALIDSOCKET(s)        ((s) > 0)
#define CLOSESOCKET(s)          close(s)
#define GETSOCKETERROR()        (errno)
#define SOCKET                  int

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IP_ADDRESS              "127.0.0.1"
#define PORT                    "8080"




int main (int argc, char *argv[])
{
    #if defined(_WIN32)
    WSADATA d;
    if (WSAStartup(MAKEWORD(2, 2), &d))
    {
        fprintf(stderr, "Failed to initialize winsock library for window.\n");
        exit(EXIT_FAILURE);
    }
    #endif

    /* *********************** */
    printf("Configuring address ...\n");
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_socktype   = SOCK_DGRAM;       // UDP

    struct addrinfo *peer_address;
    getaddrinfo(IP_ADDRESS, PORT, &hints, &peer_address);

    /* *************************************************** */
    char address_buffer[100];
    char port_buffer[100];
    getnameinfo(peer_address->ai_addr, peer_address->ai_addrlen, address_buffer, sizeof(address_buffer), port_buffer, sizeof(port_buffer), NI_NUMERICHOST | NI_NUMERICSERV);
    printf("Remote address is: %s, %s\n", address_buffer, port_buffer);


    /* ****************************** */
    printf("Creating socket ...\n");
    SOCKET socket_peer = socket(peer_address->ai_family, peer_address->ai_socktype, peer_address->ai_protocol);
    if (!ISVALIDSOCKET(socket_peer))
    {
        fprintf(stderr, "socket() failed (%d).\n", GETSOCKETERROR());
        exit(EXIT_FAILURE);
    }


    /* ***************************** */
    char sends[] = "Hello World";
    ssize_t byte_sent = sendto(socket_peer, sends, strlen(sends), 0, peer_address->ai_addr, peer_address->ai_addrlen);
    printf("Sent: %d bytes.\n", byte_sent);
    printf("Content: %s\n", sends);

     /* ******************************* */
    CLOSESOCKET(socket_peer);


    /* ******************************** */
    #if defined(_WIN32)
    WSACleanup();
    #endif

    printf("Finished.\n");

    exit(EXIT_SUCCESS);
}

