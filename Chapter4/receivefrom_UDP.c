#if defined(_WIN32)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#include <winsock2.h>
#include <ws2tcpip.h>

#define ISVALIDSOCKET(s)        ((s) != INVALID_SOCKET)
#define CLOSESOCKET(s)          closesocket(s)
#define GETSOCKETERROR()        GetLastError()

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#define ISVALIDSOCKET(s)        ((s) > 0)
#define CLOSESOCKET(s)          close(s)
#define GETSOCKETERROR()        (errno)
#define SOCKET                  int

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IP_ADDRESS              "127.0.0.1"
#define PORT                    "8080"




int main (int argc, char *argv[])
{
    #if defined(_WIN32)
    WSADATA d;
    if (WSAStartup(MAKEWORD(2, 2), &d))
    {
        fprintf(stderr, "Failed to initialize winsock library for window.\n");
        exit(EXIT_FAILURE);
    }
    #endif

    /* *********************** */
    printf("Configuring address ...\n");
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_socktype   = SOCK_DGRAM;       // UDP
    hints.ai_family     = AF_INET;          // IPv4
    hints.ai_flags      = AI_PASSIVE;

    struct addrinfo *bind_address;
    getaddrinfo(IP_ADDRESS, PORT, &hints, &bind_address);


    /* ****************************** */
    printf("Creating socket ...\n");
    SOCKET socket_listen = socket(bind_address->ai_family, bind_address->ai_socktype, bind_address->ai_protocol);
    if (!ISVALIDSOCKET(socket_listen))
    {
        fprintf(stderr, "socket() failed (%d).\n", GETSOCKETERROR());
        exit(EXIT_FAILURE);
    }


    /* ***************************** */
    printf("Binding socket to local address ...\n");
    if (bind(socket_listen, bind_address->ai_addr, bind_address->ai_addrlen))
    {
        fprintf(stderr, "bind() failed (%d).\n", GETSOCKETERROR());
        exit(EXIT_FAILURE);
    }
    freeaddrinfo(bind_address);


    /* ***************************** */
    struct sockaddr client_address;
    ssize_t client_length = sizeof(client_address);
    char read[1024];

    ssize_t byte_received = recvfrom(socket_listen, read, 1024, 0, &client_address, &client_length);
    printf("Received: %d bytes.\n", byte_received);
    printf("Content: %s\n", read);

    char address_buffer[100];
    char port_buffer[100];
    getnameinfo(&client_address, client_length, address_buffer, sizeof(address_buffer), port_buffer, sizeof(port_buffer), NI_NUMERICHOST | NI_NUMERICSERV);
    printf("Remote address is: %s, %s\n", address_buffer, port_buffer);

    /* ******************************* */
    CLOSESOCKET(socket_listen);


    /* ******************************** */
    #if defined(_WIN32)
    WSACleanup();
    #endif

    printf("Finished.\n");

    exit(EXIT_SUCCESS);
}

